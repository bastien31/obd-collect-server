import floatextras
import overpy
import threading

from flask import Flask
from flask import request
from pymongo import MongoClient, GEOSPHERE

APP = Flask(__name__)
OVERPY = overpy.Overpass()
OVERPY_QUERY = """
        way(around: {radius}, {latitude}, {longitude}) ["maxspeed"];
            (._;>;);
                out body;
                    """

RADIUS = "1000"  # meters
MAX_SPEED_COL = MongoClient("mongodb://localhost:27018").SPEED.max


def get_max_speed(lat, lon):
    assert (lat)
    assert (lon)
    max_speed = -1

    query = {"loc": {
        "$near": {
            "$geometry": {
                "type": "Point",
                "coordinates": [float(lon), float(lat)]
            },
            "$maxDistance": 100
        }
    }}
    speed_limits = list(MAX_SPEED_COL.find(query).limit(3))

    if not speed_limits:
        print("Retrieve data from overpy...")
        try:
            result = OVERPY.query(
                OVERPY_QUERY.format(
                    radius=RADIUS, latitude=lat, longitude=lon))
        except:
            result = None
            threading.sleep(1)

        if result:
            limitations = []
            for way in result.ways:
                max_speed = way.tags.get("maxspeed", -1)
                for node in way.nodes:
                    limitations.append({
                        "loc": {
                            "coordinates": [floatextras.next_plus(node.lon),
                                            floatextras.next_plus(node.lat)],
                            "type": "Point"
                        },
                        "maxSpeed": max_speed
                    })

            if limitations:
                MAX_SPEED_COL.create_index([("loc", GEOSPHERE)])
                MAX_SPEED_COL.insert_many(limitations)

            speed_limits = list(MAX_SPEED_COL.find(query).limit(3))

            if not speed_limits:
                limitations = []
                limitations.append({
                    "loc": {
                        "coordinates": [floatextras.next_plus(float(lon)),
                                        floatextras.next_plus(float(lat))],
                        "type": "Point"
                    },
                    "maxSpeed": -1
                })
                MAX_SPEED_COL.create_index([("loc", GEOSPHERE)])
                MAX_SPEED_COL.insert_many(limitations)


    if speed_limits and all(x["maxSpeed"] == speed_limits[0]["maxSpeed"]
                            for x in speed_limits):
        max_speed = speed_limits[0]["maxSpeed"]

    return int(max_speed)


@APP.route('/')
def is_overspeed():
    latitude = request.args.get('latitude')
    longitude = request.args.get('longitude')
    speed = int(request.args.get('speed'))

    max_speed = get_max_speed(latitude, longitude)

    if max_speed == -1:
        return "n/a"
    elif speed <= max_speed:
        return "ok"
    else:
        return "ko"


if __name__ == "__main__":
    APP.run(debug=False, host= '0.0.0.0')
