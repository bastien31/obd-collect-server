#!/bin/bash

spark-submit \
--master yarn-client \
--conf "spark.mongodb.input.uri=mongodb://localhost/OBD.obd?readPreference=primaryPreferred" \
--conf "spark.mongodb.output.uri=mongodb://localhost/OBD.obd" \
--packages org.mongodb.spark:mongo-spark-connector_2.10:1.0.0 \
--jars spark-streaming-mqtt-assembly_2.11-1.6.2.jar \
obd_stream.py "localhost" 1883

#--driver-java-options "-Dlog4j.configuration=file:///app/log4j.properties" \
