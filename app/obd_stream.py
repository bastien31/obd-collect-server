import json
import sys
import urllib2

from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql import functions
from pyspark.sql.types import BooleanType
from pyspark.streaming import StreamingContext
from pyspark.streaming.mqtt import MQTTUtils

sc = SparkContext(appName="ObdStream")
sc.setLogLevel("ERROR")
sqlContext = SQLContext(sc)
ssc = StreamingContext(sc, 1)

mqqt_server_ip = sys.argv[1]
mqqt_server_port = sys.argv[2]

def is_overspeed(lat, lon, speed):
    overspeed = False
    url = "http://localhost:5000/?latitude={lat}&longitude={lon}&speed={speed}".format(lat=lat, lon=lon, speed=speed)

    try:
        result = urllib2.urlopen(url).read()
        overspeed = not(result in ["ok", "n/a"])
    except:
        pass

    return overspeed

dt_overspeed = functions.udf(is_overspeed, BooleanType())

def store_rdd(rdd):
    if len(rdd.take(1)) != 0:
        df = sqlContext.jsonRDD(rdd)
        # df.printSchema()

        new_df = df.withColumn("overspeed",
                                dt_overspeed(df.location.geoPoint.coordinates[1],
                                             df.location.geoPoint.coordinates[0],
                                             df.obdData.speed.value))
        # new_df.show()
        new_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").save()


mqttStream = MQTTUtils.createStream(ssc,
                                    "tcp://{0}:{1}".format(mqqt_server_ip,
                                                          mqqt_server_port),
                                    "#")
mqttStream.foreachRDD(store_rdd)

ssc.start()
ssc.awaitTermination()
