import json
import time
import random
import paho.mqtt.client as mqtt_client

WHO_I_AM = "moto1"

DATA = {
    "location" : {
        "altitude" : 0.0,
        "geoPoint" : {
            "coordinates" : [
                1.4789938,
                43.5787952
            ],
            "type" : "Point"
        }
    },
    "obdData" : {
        "speed" : {
            "unit" : "km/h",
            "value" : "0"
        }
    },
    "time" : "2016-10-13T11:10:27+02:00",
    "vin" : "titi"
}


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc):
    print("Connected with result code: {}".format(rc))


def on_disconnect(client, userdata, rc):
    print("Disconnected with result code: {}".format(rc))


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("{}: {}".format(msg.topic, msg.payload))


def main():
    client = mqtt_client.Client(client_id=WHO_I_AM, clean_session=False)

    client.max_inflight_messages_set(100)

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect

    # client.connect("127.0.0.1")
    client.connect_async("127.0.0.1")
    # client.connect_async("vps311898.ovh.net")
    client.loop_start()

    while True:
        DATA["obdData"]["speed"]["value"] = random.randint(0, 160)
        DATA["vin"] = random.choice(["titi", "toto"])
        client.publish(
            topic="{}".format(WHO_I_AM),
            payload=json.dumps(DATA),
            qos=1)
        time.sleep(1)

    client.loop_stop()
    client.disconnect()
    print("exit")
    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    # client.loop_forever()


if __name__ == '__main__':
    main()
